#! /bin/bash

######
# Script de setup Steam
#
# Auteur ? o_be_one de r0x.fr
# Howto ? Editer, executer et repondre aux questions.
######


### Variables a editer :

# Informations utilisateur
USER="user"
PASS="pass"

# Emplacement de Steam :
STEAM=~/steamcmd

# Ou installer le jeu ?
WHERE=~

# Quel jeu ?
GAME="7dtd"
ID="294420"

### Editer ce qui suit a vos risques ...

echo -e "\nNous devons surement supprimer l'ancien backup ..."
reat -p "Appuyez sur [ENTREE] pour supprimer l'ancien backup ..."

rm -rf $WHERE/$GAME/BACKUP

echo -e "\nOk, mise a jour de $GAME (ID : $ID) dans $WHERE/$GAME ..."
read -p "Appuyez sur [ENTREE] pour demarrer l'installation ..."

mkdir -p $WHERE/$GAME/BACKUP
cp $WHERE/$GAME/serveradmin.xml $WHERE/$GAME/BACKUP
cp $WHERE/$GAME/serverconfig.xml $WHERE/$GAME/BACKUP
cp -rf "$WHERE/7 Days To Die" $WHERE/$GAME/BACKUP

cd $STEAM && ./steamcmd.sh +login $USER $PASS +force_install_dir $WHERE/$GAME +app_update $ID $SUPP +exit && cd $WHERE/$GAME